# Algo-devops

## Description

The algo-devops repository is part of the Algo project.  
This repository is intended as a source of various templates and configurations required for CI/CD of other
repositories of the Algo project.

## Style-check

Although this repository mainly contains configuration files .yml or .yaml files, which can not be executed,
it is possible to check the files for compliance with a single style.

Run style-check:

* Local machine:
    1. Run `npm install --save-dev`
    2. Run `npm run style`
* Docker:
    1. Run `docker build -t algo-devops .`
    2. Run `docker run --rm --name algo-devops algo-devops`

## Contributing

Contributes to this project must follow approved gitflow.

### Branches & Tags

#### The master branch

* The **master** branch contains the latest version.
* Source branch for the **master** branch is only the **develop** branch.

#### The tags

* Previous versions are available through tags.
* Versions have a format: MAJOR.MINOR.PATCH e.g.: 1.3.2.
* Only the last PATCH version is stored as tag, e.g., 1.3.2 is stored, but 1.3.1 is deprecated and removed from the git.

#### The develop branch

* The **develop** branch is used for developing.

#### The other branches

* The dev branches should start from the **develop** branch.
* The dev branches should have prefix *feature/* or *fix/*.
* Any branch name should follow kebab-case.
* The dev branches should end in the **develop** branch with merge request.
* Merge request of the dev branch to develop branch should delete this dev branch after accept.

### Other rules

* Do not use rebase.
* Do not use squashing.

## Authors

- Michael Linker - [work.michaellinker@gmail.com](mailto:work.michaellinker@gmail.com)