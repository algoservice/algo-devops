# Global ARG
ARG PROJECT_DIRECTORY=/tmp/algo-devops

# 1: Build React app
FROM node:20-alpine AS develop

ARG PROJECT_DIRECTORY
WORKDIR $PROJECT_DIRECTORY

# Prepare production dependencies
COPY package.json .
RUN npm install --save-dev

# Prepare project files
COPY . .

CMD ["npm", "run", "style"]